<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['id_number','id_state','name','lastname','address','phone'];

    public function travels() { 
    	return $this->belongsToMany(Travel::class)->withTimestamps(); 
    }
}
