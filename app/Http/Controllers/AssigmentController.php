<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Travel;
use App\State;

class AssigmentController extends Controller {

    public function store (Request $request) {

        $travel  = Travel::find(request('id_travel'));
        $clients = $travel->clients->toArray();
        if (count($clients)>0){          
            foreach ($travel->clients as $client) 
           {
              if($client->id == request('id_client')){
                return response()->json([
                        'status' => 'Ocurrio un error!',
                        'msg' => 'Este Viajero ya esta asociado a este Viaje.',
                    ],400); 

              }else{
                $travel->clients()->attach(request('id_client'));
                $travel->places  = $travel->places-1;                 
                $travel->save();
                return;

              }
           }
          }else{
            $travel->clients()->attach(request('id_client'));
                $travel->places  = $travel->places-1;                 
                $travel->save();
                return;      
          }
    }

    public function show ($id) {

        $travel= Travel::find($id);
        $clientsAssign = $travel->clients->toArray();

        return $clientsAssign;
    }

    public function destroy (Request $request) {
        $travel  = Travel::find(request('id_travel'));
        $travel->clients()->detach(request('id_client'));
        $travel->places  = $travel->places+1;                 
        $travel->save();
        return;        
    }

    
}
