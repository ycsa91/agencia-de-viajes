<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Country;
use App\State;

class ClientController extends Controller {

    public function index () {

        $clients = Client::select('clients.*', 'states.state as state', 'countries.id as id_country', 'countries.country as country')
                        ->leftJoin('states', 'clients.id_state', '=', 'states.id')  
                        ->leftJoin('countries', 'states.id_country', '=', 'countries.id') 
                        ->orderBy('id_number', 'desc')                        
                        ->get();
        
        return ['clients' => $clients];
    }

    public function create () {

        $countries = Country::orderBy('country')->get();
        $states = State::orderBy('state')->get();
        
        return ['countries' => $countries, 'states' => $states];
    }    

    public function store (Request $request) {

        $rules = [
           'id_number' => 'required|unique:clients,id_number',
           'id_state'  => 'required',
           'name'      => 'required',
           'lastname'  => 'required',
           'address'   => 'required',
           'phone'     => 'required'
        ];
         
        $messages = [
            'id_number.required' => 'Debe ingresar su Cédula.',
            'id_number.unique'   => 'Éste registro ya Existe.',
            'name.required'      => 'Debe ingresar sus Nombres.',
            'lastname.required'  => 'Debe ingresar sus Apellidos.',
            'id_state.required'  => 'Debe ingresar su Estado de Procedencia.',
            'address.required'   => 'Debe ingresar su Dirección de Habitación.',
            'phone.required'     => 'Debe ingresar s Número Telefónico.'
            
        ];

        $this -> validate($request, $rules, $messages);

        $client = new Client([
            'id_number' => request('id_number'),
            'id_state'  => request('id_state'),
            'name'      => mb_strtoupper(request('name'),'utf-8'),
            'lastname'  => mb_strtoupper(request('lastname'),'utf-8'),            
            'address'   => mb_strtoupper(request('address'),'utf-8'),
            'phone'     => request('phone')            
          ]);

        $client->save();
        return ;
    }

    public function update (Request $request, $id) {

        $rules = [
           'id_number' => 'required|unique:clients,id_number,'.$id,
           'id_state'  => 'required',
           'name'      => 'required',
           'lastname'  => 'required',
           'address'   => 'required',
           'phone'     => 'required'
        ];
         
        $messages = [
            'id_number.required' => 'Debe ingresar su Cédula.',
            'id_number.unique'   => 'Éste registro ya Existe.',
            'name.required'      => 'Debe ingresar sus Nombres.',
            'lastname.required'  => 'Debe ingresar sus Apellidos.',
            'id_state.required'  => 'Debe ingresar su Estado de Procedencia.',
            'address.required'   => 'Debe ingresar su Dirección de Habitación.',
            'phone.required'     => 'Debe ingresar su Número Telefónico.'
            
        ];

        $this -> validate($request, $rules, $messages);

        $client = Client::findOrFail($id);

        $client->id_number = request('id_number');
        $client->id_state  = request('id_state');
        $client->name      = mb_strtoupper(request('name'),'utf-8');
        $client->lastname  = mb_strtoupper(request('lastname'),'utf-8');
        $client->address   = mb_strtoupper(request('address'),'utf-8');
        $client->phone     = request('phone');
        
        $client->save();
    }

    public function destroy ($id) {
        $client = Client::findOrFail($id);
        $client->delete();
        
    }

    
}
