<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Travel;
use App\State;

class TravelController extends Controller {

    public function index () {

        $travels = Travel::select('travels.*', 'f.state as from', 't.state as to')
                        ->leftJoin('states as f', 'travels.id_from', '=', 'f.id')                 
                        ->leftJoin('states as t', 'travels.id_to', '=', 't.id')                 
                        ->orderBy('created_at', 'desc')                        
                        ->get();
        
        return ['travels' => $travels];
    }

    public function create () {

        $states = State::orderBy('state')->get();
        
        return ['states' => $states];
    }    

    public function store (Request $request) {

        $rules = [
           'code'    => 'required|unique:travels,code',
           'id_from' => 'required',
           'id_to'   => 'required',
           'places'  => 'required',
           'price'   => 'required'
        ];
         
        $messages = [
            'code.required'    => 'Debe ingresar el Código.',
            'code.unique'      => 'Éste registro ya Existe.',
            'id_from.required' => 'Debe ingresar el Origen.',
            'id_to.required'   => 'Debe ingresar el Destino.',
            'places.required'  => 'Debe ingresar la cantidad de Plazas.',
            'price.required'   => 'Debe ingresar el Precio.'            
        ];

        $this -> validate($request, $rules, $messages);

        $travel = new Travel([
            'code'    => mb_strtoupper(request('code'),'utf-8'),
            'id_from' => request('id_from'),
            'id_to'   => request('id_to'),
            'places'  => request('places'),            
            'price'   => str_replace(',','',request('price'))
          ]);

        $travel->save();
        return ;
    }

    public function update (Request $request, $id) {

        $rules = [
           'code'    => 'required|unique:travels,code,'.$id,
           'id_from' => 'required',
           'id_to'   => 'required',
           'places'  => 'required',
           'price'   => 'required'
        ];
         
        $messages = [
            'code.required'    => 'Debe ingresar el Código.',
            'code.unique'      => 'Éste registro ya Existe.',
            'id_from.required' => 'Debe ingresar el Origen.',
            'id_to.required'   => 'Debe ingresar el Destino.',
            'places.required'  => 'Debe ingresar la cantidad de Plazas.',
            'price.required'   => 'Debe ingresar el Precio.'            
        ];

        $this -> validate($request, $rules, $messages);


        $travel = Travel::findOrFail($id);

        $travel->code    = mb_strtoupper(request('code'),'utf-8');
        $travel->id_from = request('id_from');
        $travel->id_to   = request('id_to');
        $travel->places  = request('places');            
        $travel->price   = str_replace(',','',request('price'));        
        $travel->save();
        return;
    }

    public function destroy ($id) {
        $travel = Travel::findOrFail($id);
        $travel->delete();
        
    }

    
}
