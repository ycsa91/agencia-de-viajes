<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Travel extends Model
{
    protected $fillable = ['code','id_from','id_to','places','price'];
    
    public function clients() { 
    	return $this->belongsToMany(Client::class)->withTimestamps(); 
    }
}
