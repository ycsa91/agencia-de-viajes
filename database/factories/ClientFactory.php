<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
    	'id_number'   => $faker->numberBetween(100000,200000000),
        'id_state'    => $faker->numberBetween(1,15),
        'name'        => $faker->name,
        'lastname'    => $faker->lastName,
        'address'     => $faker->address,
        'phone'       => $faker->phoneNumber,
    ];
});
