<?php

use Faker\Generator as Faker;

$factory->define(App\State::class, function (Faker $faker) {
    return [
        'id_country' => $faker->numberBetween(1,5),
        'state'      => $faker->sentence,
    ];
});
