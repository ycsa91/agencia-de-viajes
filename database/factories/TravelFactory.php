<?php

use Faker\Generator as Faker;

$factory->define(App\Travel::class, function (Faker $faker) {
    return [
        'code'    => $faker->word,
        'id_from' => $faker->numberBetween(1,15),
        'id_to'   => $faker->numberBetween(1,15),
        'places'  => $faker->numberBetween(20,100),
        'price'   => $faker->randomFloat($nbMaxDecimals = 2, $min = 20, $max = 200),
    ];
});
