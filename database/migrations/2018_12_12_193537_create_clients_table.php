<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_number')->unique();
            $table->unsignedInteger('id_state');
            $table->string('name', 50);
            $table->string('lastname', 50);
            $table->string('address',200);
            $table->string('phone',25);                       
            $table->timestamps();

            $table->foreign('id_state')
            ->references('id')->on('states')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
