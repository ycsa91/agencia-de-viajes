import Vue from 'vue';
import Router from 'vue-router';
import Wellcome from './components/WellcomeComponent';
import Client from './components/ClientsComponent';
import Travel from './components/TravelsComponent';
import Assignment from './components/AssigmentsComponent';

import Php from './components/404';


Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'wellcome',
            component: Wellcome,
            props: true
        },
        {
            path: '/clients',
            name: 'clients',
            component: Client,
            props: true
        },
        
        {
            path: '/travels',
            name: 'travels',
            component: Travel,
            props: true
        },
        {
            path: '/assignments',
            name: 'assignments',
            component: Assignment
            //props: true
        },
        {
            path: '*',
            component: Php
            //props: true
        }
    ],
    linkExactActiveClass: 'active',
    mode: 'history',
    scrollBehavior(){
        return {x:0, y:0}
    }
});