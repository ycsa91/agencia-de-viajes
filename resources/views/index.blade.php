<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>AGENCIA DE VIAJES</title>
  <!-- Bootstrap core CSS-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <!-- <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
  <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

  <!-- Custom styles for this template-->
  <!-- <link href="css/sb-admin.css" rel="stylesheet"> -->
  <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <div id="app">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="/">AGENCIA DE VIAJES</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive" >
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion" style="overflow: auto !important;">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Inicio">
          <router-link class="nav-link" to="/">
            
            <span class="nav-link-text">Inicio</span>
          </router-link>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Clientes">
          <router-link class="nav-link" to="/clients">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Clientes</span>
          </router-link>
        </li>   
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Viajes">
          <router-link class="nav-link" to="/travels">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Viajes</span>
          </router-link>
        </li>  
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Asignación">
          <router-link class="nav-link" to="/assignments">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Asignación</span>
          </router-link>
        </li>   
      </ul>    
      
      <ul class="navbar-nav ml-auto">
        
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Salir</a>
        </li>
      </ul>
    </div>
  </nav>
  <div  class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
            
            <router-view></router-view>
        </div>
      </div>
    </div>
</div>
</div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © AGENCIA DE VIAJES 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cerrar Sesión</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <form id="logout-form" action="" method="POST">              
            @csrf;          
          <div class="modal-body">¿Está seguro que desea abandonar la página?</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Si</button>
          </div>
          </form>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{ asset('js/jquery.easing.min.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin.min.js') }}"></script>

    <script src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('.date').mask('00/00/0000');
            $('.phone').mask('(00) 000 000-0000');
            $('.price').mask("#,##0.00", {reverse: true}); 
            $('.number').mask('999'); 
            $('.id_number').mask('9.999.999.999'); 
        })
    </script>

</body>
</html>
