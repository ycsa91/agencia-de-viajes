<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//Rutas para el módulo de Viajeros
Route::resource('traveler','ClientController',['except' => 'show', 'edit']);

//Rutas para el módulo de Viajes
Route::resource('trip','TravelController',['except' => 'show', 'edit']);

//Rutas para el módulo de Asignación
Route::resource('assign','AssigmentController');
Route::post('assign_delete', 'AssigmentController@destroy');

//SPA
Route::any('/{any?}', function () {
    return view('index');
});